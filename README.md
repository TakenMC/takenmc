<div align="center">
    <h1>Taken</h1>
    <h3>I do random stuff and other things</h3>
    <h2>Personal projects</h2>
    <a href="https://gitlab.com/illegitimate/illegitimate-bot">
        <img src="https://img.shields.io/gitlab/last-commit/illegitimate%2Fillegitimate-bot?style=for-the-badge&logo=gitlab&label=Illegitimate%20Bot&color=eeaadb">
    </a>
    <h2>Other links</h2>
    <a href="https://mairimashita.org/">
        <img src= "https://img.shields.io/website?url=https%3A%2F%2Fmairimashita.org%2F&style=for-the-badge&logo=hugo&label=Website&color=eeaadb">
    </a>
    <a href="https://discord.gg/hGjVrPBThq">
        <img src= "https://img.shields.io/discord/274183680245628928?style=for-the-badge&logo=discord&color=eeaadb">
    </a><br>
    <a href="https://sakurajima.moe/@taken">
        <img src= "https://img.shields.io/mastodon/follow/110719192441732476?domain=https%3A%2F%2Fsakurajima.moe%2F&style=for-the-badge&logo=mastodon&label=Mastodon&color=eeaadb">
    </a>
    <a href="https://x.com/igntakie">
        <img src= "https://img.shields.io/twitter/follow/igntakie?style=for-the-badge&logo=x&label=X%20(formerly%20Twitter)&color=eeaadb">
    </a>
</div>